<?php 

// Create a Person class with three properties: firstName, middleName, and lastName.
// Create two child classes for Person: Developer and Engineer
// Create a printName() method in each of the classes and must have the following output:
// Person "Your full name is Senku G. Ishigami"
// Developer: " Your name is John Finch Smith and you are a developer"
// Engineer: "You area an engineer named Harold Myers Reese."
class Person {

	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}
}

$person = new Person('Senku', 'G.', 'Ishigami');

class Developer extends Person{
	public $occupation;

	public function __construct($firstName, $middleName, $lastName, $occupation){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
		$this->occupation =$occupation;
	}

	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a $this->occupation.";
	}

}

$developer = new Developer('John', 'Finch', 'Smith', 'developer');

class Engineer extends Person{
	public $occupation;

	public function __construct($firstName, $middleName, $lastName, $occupation){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
		$this->occupation =$occupation;
	}

	public function printName(){
		return "You are an $this->occupation named $this->firstName $this->middleName $this->lastName.";
	}
}

$engineer = new Engineer('Harold', 'Myers', 'Reese', 'Engineer');





 ?>