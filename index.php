<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<title>S03: Activity</title>
	</head>
	<body>
		<h1>Person</h1>
		<p>Person: <?php echo $person->printName(); ?></p>

		<h1>Developer</h1>
		<p>Developer: <?php echo $developer->printName(); ?></p>

		<h1>Engineer</h1>
		<p>Engineer: <?php echo $engineer->printName(); ?></p>

	</body>
</html>
